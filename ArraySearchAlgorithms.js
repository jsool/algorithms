/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// return matching position within values or values.length if not found.
function BinarySearch(values, value_to_find)
{
    var start = 0;
    var end;
    var mid;

    if (values.length > 0)
    {
        end = values.length - 1;

        do
        {
            mid = (start + end) >> 1;
            var to_compare = values[mid];
            if (value_to_find < to_compare)
                end = mid - 1;
            else if (value_to_find > to_compare)
                start = mid + 1;
            else
                break;
        }
        while (start <= end);
        return (start <= end) ? mid : values.length;
    }

    return 0;
}
